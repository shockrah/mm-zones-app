use crate::db::get_zones_list;

use mysql_async::{self, Pool};
use hyper::{self, Body, Response, StatusCode,
    header::HeaderValue
};
use crate::db::get_zone_data;


pub async fn get_zones(p: &Pool, response: &mut Response<Body>) -> Result<(), mysql_async::error::Error> {
    /*
     * Here is where we need to get a list of tuples that look like so:
     * (mode, filename, checksum)
     * There is no particular order for this data so its up to the user to deal
     * NOTE: no filtering done because really there won't ever be too many zone files
     * /maybe a few hunderd for the foreseeable future/
    */

    let loaded_zones = get_zones_list(p.get_conn().await?).await?;

    let mut new_body = "{\"zones\":[".to_string();
    for zone in loaded_zones.iter() {
        new_body.push_str(&format!("{},", zone.itemize()));
    }
    if new_body.ends_with(',') {
        new_body.pop();
    }
    new_body.push_str("]}");

    *response.body_mut() = Body::from(new_body);
    response.headers_mut().insert("Content-Type", HeaderValue::from_static("application/json"));
    Ok(())
}

fn get_param(qs: &str, name: &'static str) -> Option<String> {
    /*
     * We can safely assume that we have a valid query string of some kidn
     * Our task is to determine if the request parameter(name) is in there
    */
    // Split the thing into piecese
    let pairs: Vec<&str> = qs.split("&").collect();
    match pairs.len() {
        0 => None,
        _ => {
            let mut found = false;
            let mut val = String::new();
            for pair in pairs.iter() {
                let sub_seg_v: Vec<&str> = pair.split("=").collect();
                let (key, value) = match sub_seg_v.len() {
                    2 => (sub_seg_v[0], sub_seg_v[1]),
                    _ => ("", "")
                };
                if key == name {
                    val.push_str(value);
                    found = true;
                    break;
                }
            }

            if found {Some(val)}
            else {None}
        }
    }
}

pub async fn download_zone(p: &Pool, response: &mut Response<Body>, query_str: &str) -> Result<(), mysql_async::error::Error> {
    /* 
     * Zone data is stored in a TEXT field in the schema so we simply populate
     * the body of the response with our text data
    */
    match get_param(query_str, "zone") {
        Some(zone) => {
            let disp = format!("attachment; filename={}", zone);
            let data = get_zone_data(p.get_conn().await?, zone).await?;
            *response.status_mut() = StatusCode::OK;


            response.headers_mut().insert("Content-type", HeaderValue::from_static("text/plain"));
            response.headers_mut().insert("Accept-Ranges" , HeaderValue::from_static("bytes"));
            response.headers_mut().insert("Content-Disposition", HeaderValue::from_str(&disp).expect("Could not add content disposition"));

            *response.body_mut() = Body::from(data);
            Ok(())
        }
        None => {
            *response.status_mut() = StatusCode::BAD_REQUEST;
            Ok(())
        }
    }
}


#[cfg(test)]
mod api_unit_tests{
    use crate::api::get_param;

    #[test]
    fn test_getting_param_qs() {
        let qs = "key=value&&&&this=one";
        let result = get_param(qs, "this");
        println!("{:?}", result);

        assert_eq!(result, Some("one".to_string()))
    }
}