use std::convert::Infallible;
use std::net::SocketAddr;
use std::env;

use tokio::{self, fs};

use mysql_async::Pool;

use hyper::{
    self, Body, Request,
    Method, Server,
    Response, StatusCode,
    service:: {
        make_service_fn, service_fn
    }
};

use dotenv::dotenv;

mod api;
mod db;

async fn get_index_html() -> String {
    let fd = fs::read_to_string("web/index.html").await;
    match fd {
        Ok(data) => data,
        Err(e) => panic!("Couldn't get web content somethings fucked: {}", e)
    }
}

async fn response_dispatcher(request: Request<Body>) -> Result<Response<Body>, hyper::Error>{
    let mut response = Response::new(Body::empty());
    let web_content = get_index_html().await;
    let pool = Pool::new(&env::var("DATABASE_URL").unwrap());

    println!("{}=>{}", request.method(), request.uri().path());
    match(request.method(), request.uri().path()) {

        /* Home page with the web ui */
        (&Method::GET, "/") => {
            *response.status_mut() = StatusCode::OK;
            *response.body_mut() = Body::from(web_content);
        },
        /* list of zones from our db */
        (&Method::GET, "/api/list") => {
            if let Err(e) = api::get_zones(&pool, &mut response).await {
                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                println!("/api/list INTERNAL_SERVER_ERROR: {} | ", e);
            }
        },
        /* download a particular zone based off the parameter @zone*/
        (&Method::GET, "/api/download_zone") => {
            match request.uri().query() {
                Some(qs) => {
                    // 500 errors are dealt with here but thats about it
                    if let Err(e) = api::download_zone(&pool, &mut response, qs).await {
                        *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                        println!("/api/zone INTERNAL_SERVER_ERROR: {} |", e);
                    }
                },
                None => *response.status_mut() = StatusCode::BAD_REQUEST
            }
        },
        _ => *response.status_mut() = StatusCode::NOT_FOUND
    }
    // yea tbf we don't really care what happens at this point since we can just do whatever from here
    match pool.disconnect().await {
        Ok(_) => {},
        Err(_) => {*response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;}
    }
    Ok(response)
}

async fn shutdown_signal() {
    tokio::signal::ctrl_c()
        .await
        .expect("failed to shutdown normally");
}

#[tokio::main]
async fn main() {
    dotenv().ok();
    println!("Serving on localhost:8080");
    let addr = SocketAddr::from(([127,0,0,1], 8080));

    let service = make_service_fn(|_conn| async {
        Ok::<_, Infallible>(service_fn(response_dispatcher))
    });

    let server = Server::bind(&addr).serve(service);
    let graceful_shutdown = server.with_graceful_shutdown(shutdown_signal());

    if let Err(e) = graceful_shutdown.await {
        eprintln!("Server error: {}", e);
    }
}
