// Module for dealing with data from our db
use std::borrow::Cow;
use mysql_async::prelude::{Queryable, params};
use mysql_async::Conn;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ListRow {
    pub mode: String,
    pub mapname: String, 
    pub checksum :String,
}

impl ListRow {
    pub fn itemize(&self) -> String {
        /*
         * Used to turn the struct into a valid json tuple which does not 
         * use () brackets but [] brackets
        */
        format!("[\"{}\",\"{}\",\"{}\"]", self.mode, self.mapname, self.checksum)
    }
}

pub async fn get_zones_list(conn: Conn) -> Result<Vec<ListRow>, mysql_async::error::Error> {

    let zones = conn.prep_exec(r"SELECT mode, mapname, _checksum FROM zones ORDER BY mapname", ()).await?;
    let (_, loaded_zones) = zones.map_and_drop(|row| {
        let (mode, mapname, checksum) = mysql_async::from_row(row);
        ListRow {
            mode: mode,
            mapname: mapname,
            checksum: checksum
        }
    }).await?;

    Ok(loaded_zones)
}

pub async fn get_zone_data(conn: Conn, name: String) -> Result<String, mysql_async::error::Error> {
    // first_exec hangs so for now we just use prep_exec which we use to later
    // build a vec of strings dropping everything that isn't a vector of one item
    //let (_conn, zone): (Conn, Option<String>) = conn.first_exec("SELECT zdata FROM zones WHERE mapname = :mname", params!{"mname"=>name}).await?;
    let db_rows = conn.prep_exec(r"SELECT zdata FROM zones WHERE mapname = :mname", params!{
        "mname" => name
    }).await?;
    
    let (_, zrows): (Conn, Vec<String>) = db_rows.map_and_drop(|row| {
        mysql_async::from_row(row)
    }).await?;

    match zrows.len() {
        1 => Ok(zrows[0].clone()),
        _ => {
            let x = Cow::from("Nothing found");
            Err(mysql_async::error::Error::Other(x))
        }
    }
}